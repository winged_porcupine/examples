#pip install starkbank-ecdsa
import base64
from ellipticcurve.ecdsa import Ecdsa
from ellipticcurve.signature import Signature
from ellipticcurve.publicKey import PublicKey
from ellipticcurve.privateKey import PrivateKey
from ellipticcurve.utils.file import File

# Read privateKey and public from PEM string in file
privateKeyPem = File.read("../ec.key")
publicKeyPem = File.read("../ec.pub")

publicKey = PublicKey.fromPem(publicKeyPem)
privateKey = PrivateKey.fromPem(privateKeyPem)

message = "test"

signature = Ecdsa.sign(message, privateKey)

# Encode Signature in base64.
encoded_signature = signature.toBase64()

print("Signature is:")
print(encoded_signature)

# to verify signature
decoded_signature = Signature.fromBase64(encoded_signature)

print("Signature valid?")
print(Ecdsa.verify(message, decoded_signature, publicKey))
