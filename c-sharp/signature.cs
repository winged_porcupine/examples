using System;
using System.IO;
using System.Text;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;
class Program
{
    static void Main()
    {
        try
        {
            var testData = "{\"key\": \"value\"}";
            var privateKeyPath = "../ec.key"
            var publicKeyPath = "../ec.pub"

            var encryptionAlgorithm = "SHA-256withECDSA";
            AsymmetricCipherKeyPair privateKey = ReadAsymmetricKeyPair(privateKeyPath);
            AsymmetricKeyParameter publicKey = ReadPublicKey(publicKeyPath);
            // Data to be signed
            byte[] data = Encoding.UTF8.GetBytes(testData);
            // Sign the data using the private key
            ISigner signer = SignerUtilities.GetSigner(encryptionAlgorithm);
            signer.Init(true, privateKey.Private);
            signer.BlockUpdate(data, 0, data.Length);
            byte[] signature = signer.GenerateSignature();
            // Verify the signature using the public key
            ISigner verifier = SignerUtilities.GetSigner(encryptionAlgorithm);
            verifier.Init(false, publicKey);
            verifier.BlockUpdate(data, 0, data.Length);
            bool isVerified = verifier.VerifySignature(signature);
            Console.WriteLine("Signature verified: " + isVerified);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error: " + ex.Message);
        }
    }
    // Helper method to read an AsymmetricCipherKeyPair from a PEM file
    private static AsymmetricCipherKeyPair ReadAsymmetricKeyPair(string filePath)
    {
        using (TextReader reader = File.OpenText(filePath))
        {
            PemReader pemReader = new PemReader(reader);
            return (AsymmetricCipherKeyPair)pemReader.ReadObject();
        }
    }
    // Helper method to read a public key from a PEM file
    private static AsymmetricKeyParameter ReadPublicKey(string filePath)
    {
        using (TextReader reader = File.OpenText(filePath))
        {
            PemReader pemReader = new PemReader(reader);
            return (AsymmetricKeyParameter)pemReader.ReadObject();
        }
    }
}