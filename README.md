# Security

To authenticate requests to and from St8 system we use **ECDSA signature** ([Elliptic Curve Digital Signature Algorithm](https://en.wikipedia.org/wiki/Elliptic_Curve_Digital_Signature_Algorithm)).


Request author has to sign payload with his private key and receiving party has to validate signature with corresponding public key.

# Interactions Diagram

The diagram below illustrates how St8 and Operator generate and exchange keys during setup phase
and signature validation later during operation phase.

# Example with OpenSSL library

## Generate private Elliptic Curve key

`openssl ecparam -genkey -name prime256v1 -noout -out ec.key`


You should have now file ec.key wit content similar to this:


```
-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIIBEdi1kkP+IAIVmbke0EP8XSyEAaEi82dwLUwr9HuY9oAoGCCqGSM49
AwEHoUQDQgAE1yliYVgjr28KYkDD0QYkGa9Y11folImZkMrOiNdmz01Eq7a1y9G8
l/KlnaDluWnKEpm0IBpNy1D+Y5jXIivvbQ==
-----END EC PRIVATE KEY-----
```

## Retrieve public key from private

`openssl ec -in ec.key -pubout -out ec.pub`


You should have now file ec.pub wit content similar to this:


```
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE1yliYVgjr28KYkDD0QYkGa9Y11fo
lImZkMrOiNdmz01Eq7a1y9G8l/KlnaDluWnKEpm0IBpNy1D+Y5jXIivvbQ==
-----END PUBLIC KEY-----
```

## Generate Signature

Let's assume that `"test"` is our payload which we want to sign. In this case we would sign it with
our private key from ec.key file and then encode it into base64.


`echo "test" | openssl dgst -sha256 -sign ec.key -binary | base64 > sign.txt`


Now you should have a `sign.text` file with contents similar to:

```
MEUCIGHSDLCDQHSPX9KXczZ3HpqZ7Spr+rNlgsJeJvr79PDxAiEArJ3ks/ZanV8fpqbNMSVgiwHV87vBXMGVV6ixCOhPtRY=
```

## Validate Signature

Now we need to decode signature from base64:


`base64 -D -i sign.txt -o sign.bin`


And feed it back into OpenSSL to validate (Note that we are using public ec.pub key for validation):


`echo "test" | openssl dgst -sha256 -verify ec.pub -signature sign.bin`


You should have received such result:

`Verified OK`
