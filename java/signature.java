import java.security.*;
import java.security.spec.*;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.*;

class ECDSAExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        String test_str = "test";
        
        PublicKey pub = publicKey();
        PrivateKey priv = privateKey();

        Signature ecdsa_priv = Signature.getInstance("SHA256withECDSA");

        ecdsa_priv.initSign(priv);

        byte[] strByte = test_str.getBytes("UTF-8");
        ecdsa_priv.update(strByte);

        byte[] realSig = ecdsa_priv.sign();
        System.out.println("Signature: " + new String(Base64.getEncoder().encode(realSig)));
        

        Signature ecdsa_pub = Signature.getInstance("SHA256withECDSA");

        ecdsa_pub.initVerify(pub);
        ecdsa_pub.update(test_str.getBytes("UTF-8"));

        boolean valid = ecdsa_pub.verify(realSig);
        
        System.out.println("Valid: " + valid);

    }

    private static PublicKey publicKey(){
        try{
            Path keyPath = Paths.get("../ec.pub");
            String key = new String(Files.readAllBytes(keyPath), Charset.defaultCharset());

            String publicKeyPEM = key
                .replace("-----BEGIN PUBLIC KEY-----", "")
                .replaceAll(System.lineSeparator(), "")
                .replace("-----END PUBLIC KEY-----", "");
        
            byte[] encoded = Base64.getDecoder().decode(publicKeyPEM);
        
            KeyFactory keyFactory = KeyFactory.getInstance("EC");
            KeySpec keySpec = new X509EncodedKeySpec(encoded);
            return keyFactory.generatePublic(keySpec);
        } catch(Exception ex){
            return null;
        }
    }

        private static PrivateKey privateKey(){
        try{
            String key = execCmd("openssl pkcs8 -topk8 -nocrypt -in ../ec.key");
            
            String privateKeyPEM = key
                .replace("-----BEGIN PRIVATE KEY-----", "")
                .replaceAll(System.lineSeparator(), "")
                .replace("-----END PRIVATE KEY-----", "");

            byte[] encoded = Base64.getDecoder().decode(privateKeyPEM);
            
            KeyFactory keyFactory = KeyFactory.getInstance("EC");
            KeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
            return keyFactory.generatePrivate(keySpec);
        } catch(Exception ex){
            return null;
        }
    }

    private static String execCmd(String cmd) throws java.io.IOException {
        java.util.Scanner s = new java.util.Scanner(Runtime.getRuntime().exec(cmd).getInputStream()).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}
