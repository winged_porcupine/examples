<?php
$data = "test";

// fetch private and public keys from file and ready them
$private_key = openssl_pkey_get_private("file://../ec.key");
$public_key = openssl_pkey_get_public("file://../ec.pub");

// compute signature
openssl_sign($data, $signature, $private_key, OPENSSL_ALGO_SHA256);

$encoded_signature = base64_encode($signature);

echo "Signature is:\n";
var_dump($encoded_signature);

//verify signature
$signature_to_verify = base64_decode($encoded_signature);

$r = openssl_verify($data, $signature_to_verify, $public_key, OPENSSL_ALGO_SHA256);

echo "Signature valid?:\n";
if($r == 1){
  echo "true";
} else {
  echo "false";
}

// free the key from memory
openssl_free_key($private_key);
openssl_free_key($public_key);
?>
