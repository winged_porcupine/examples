-module(signature).
-export([sign/1, verify/2]).

sign(Message) ->
  PrivateKey = get_key(private),
  Signature = public_key:sign(Message, sha256, PrivateKey),
  EncodedSignature = base64:encode(Signature),
  io:format("Signature is:~n"),
  io:format("~p~n", [EncodedSignature]),
  EncodedSignature.

verify(Message, Signature) ->
  PublicKey = get_key(public),
  DecodedSignature = base64:decode(Signature),
  IsValid = public_key:verify(Message, sha256, DecodedSignature, PublicKey),
  io:format("Signature valid?:~n"),
  io:format("~p~n", [IsValid]),
  IsValid.

get_key(public) ->
  public_key:pem_entry_decode(hd(public_key:pem_decode(element(2, file:read_file("../ec.pub")))));

get_key(private) ->
  public_key:pem_entry_decode(hd(public_key:pem_decode(element(2, file:read_file("../ec.key"))))).
